﻿namespace SuiteBot2.Game
{
	public class PlayerMove
	{
		public PlayerMove(int playerId, string move)
		{
			Move = move;
			PlayerId = playerId;
		}

		public override string ToString()
		{
			return "PlayerMove{" +
				"playerId=" + PlayerId +
				", move='" + Move + '\'' +
				'}';
		}

		public string Move { get; private set; }

		public int PlayerId { get; private set; }
	}
}
