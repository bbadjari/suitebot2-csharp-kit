﻿namespace SuiteBot2.Game
{
	public class Point
	{
		public Point(int x, int y)
		{
			X = x;
			Y = y;
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
				return true;

			if (obj == null || GetType() != obj.GetType())
				return false;

			Point point = (Point) obj;

			return (X == point.X && Y == point.Y);
		}

		public override int GetHashCode()
		{
			int result = X;

			result = 31 * result + Y;

			return result;
		}

		public override string ToString()
		{
			return "Point{" +
				"x=" + X +
				", y=" + Y +
				'}';
		}

		public int X { get; private set; }

		public int Y { get; private set; }
	}
}
