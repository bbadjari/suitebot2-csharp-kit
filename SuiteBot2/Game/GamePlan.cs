﻿using System.Collections.Generic;
using System.Linq;

namespace SuiteBot2.Game
{
	public class GamePlan
	{
		public GamePlan(int width, int height, ICollection<Point> startingPositions, int maxRounds)
		{
			Height = height;
			MaxRounds = maxRounds;
			StartingPositions = startingPositions.ToList();
			Width = width;
		}

		public override string ToString()
		{
			return "GamePlan{" +
				"width=" + Width +
				", height=" + Height +
				", startingPositions=" + StartingPositions +
				", maxRounds=" + MaxRounds +
				'}';
		}

		public int Height { get; private set; }

		public int MaxRounds { get; private set; }

		public List<Point> StartingPositions { get; private set; }

		public int Width { get; private set; }
	}
}
