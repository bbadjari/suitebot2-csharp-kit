﻿using System.Collections.Generic;
using System.Linq;

namespace SuiteBot2.Game
{
	public class GameRound
	{
		public GameRound(ICollection<PlayerMove> moves)
		{
			Moves = moves.ToList();
		}

		public override string ToString()
		{
			return "GameRound{" +
				"moves=" + Moves +
				'}';
		}

		public List<PlayerMove> Moves { get; private set; }
	}
}
