﻿using System.Collections.Generic;

namespace SuiteBot2.Game
{
	public class GameSetup
	{
		public GameSetup(int aiPlayerId, List<Player> players, GamePlan gamePlan)
		{
			AiPlayerId = aiPlayerId;
			GamePlan = gamePlan;
			Players = players;
		}

		public override string ToString()
		{
			return "GameSetup{" +
				"aiPlayerId=" + AiPlayerId +
				", players=" + Players +
				", gamePlan=" + GamePlan +
				'}';
		}

		public int AiPlayerId { get; private set; }

		public GamePlan GamePlan { get; private set; }

		public List<Player> Players { get; private set; }
	}
}
