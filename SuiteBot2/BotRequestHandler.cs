﻿using System;
using SuiteBot2.Ai;
using SuiteBot2.Json;
using SuiteBot2.Server;

namespace SuiteBot2
{
	public class BotRequestHandler : ISimpleRequestHandler
	{
		public BotRequestHandler(IBotAi botAi)
		{
			BotAi = botAi;
			SuppressErrorLogging = false;
		}

		public string ProcessRequest(string request)
		{
			try
			{
				return ProcessRequestInternal(request);
			}
			catch (Exception exception)
			{
				if (!SuppressErrorLogging)
				{
					Console.Error.WriteLine("Error while processing the request: " + request);
					Console.Error.WriteLine(exception.ToString());
				}

				return exception.ToString();
			}
		}

		private string ProcessRequestInternal(string request)
		{
			switch (JsonUtil.DecodeMessageType(request))
			{
				case JsonUtil.MessageType.Moves:
					return BotAi.MakeMove(JsonUtil.DecodeMovesMessage(request));
				case JsonUtil.MessageType.Setup:
					return BotAi.InitializeAndMakeMove(JsonUtil.DecodeSetupMessage(request));
				default:
					throw new ArgumentException("Unexpected request: " + request);
			}
		}

		public bool SuppressErrorLogging { get; set; }

		private IBotAi BotAi { get; set; }
	}
}
