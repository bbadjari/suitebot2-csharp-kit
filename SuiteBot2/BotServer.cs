﻿using System;
using SuiteBot2.Ai;
using SuiteBot2.Server;

namespace SuiteBot2
{
	public class BotServer
	{
		public const int DefaultPort = 9001;

		public static void Main(string[] args)
		{
			// Replace with your own bot implementation class.
			IBotAi botAi = new SampleBotAi();

			int port = DeterminePort(args);

			Console.WriteLine("Listening on port " + port);

			new SimpleServer(port, new BotRequestHandler(botAi)).Run();
		}

		private static int DeterminePort(string[] args)
		{
			if (args.Length == 1)
				return Convert.ToInt32(args[0]);
			else
				return DefaultPort;
		}
	}
}
