﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace SuiteBot2.Server
{
	public class SimpleServer
	{
		public const string ShutdownRequest = "EXIT";

		public const string UptimeRequest = "UPTIME";

		public SimpleServer(int port, ISimpleRequestHandler requestHandler)
		{
			Port = port;
			RequestHandler = requestHandler;
			ShouldShutdown = false;
			Stopwatch = new Stopwatch();
		}

		public void Run()
		{
			Stopwatch.Restart();

			TcpListener listener = null;
			TcpClient client = null;

			try
			{
				listener = new TcpListener(IPAddress.Loopback, Port);

				listener.Start();

				while (!ShouldShutdown)
				{
					client = listener.AcceptTcpClient();

					HandleRequest(client);
				}
			}
			finally
			{
				if (client != null)
					client.Close();

				if (listener != null)
					listener.Stop();
			}
		}

		private void HandleRequest(TcpClient client)
		{
			NetworkStream stream = client.GetStream();

			using (StreamReader reader = new StreamReader(stream))
			{
				string request = reader.ReadLine();

				if (string.IsNullOrWhiteSpace(request))
					return;

				if (request == ShutdownRequest)
				{
					ShouldShutdown = true;

					return;
				}

				using (StreamWriter writer = new StreamWriter(stream))
				{
					if (request == UptimeRequest)
						writer.WriteLine(Convert.ToInt32(Stopwatch.Elapsed.TotalSeconds));
					else
						writer.WriteLine(RequestHandler.ProcessRequest(request));
				}
			}

			stream.Close();
		}

		private int Port { get; set; }

		private ISimpleRequestHandler RequestHandler { get; set; }

		private bool ShouldShutdown { get; set; }

		private Stopwatch Stopwatch { get; set; }
	}
}
