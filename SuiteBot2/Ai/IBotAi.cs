﻿using SuiteBot2.Game;

namespace SuiteBot2.Ai
{
	public interface IBotAi
	{
		/// <summary>
		/// This method is called at the beginning of the game.
		/// </summary>
		/// <param name="gameSetup">Details of the game, such as the game plan dimensions, the list of players, etc.</param>
		/// <returns>The first move to play.</returns>
		string InitializeAndMakeMove(GameSetup gameSetup);

		/// <summary>
		/// This method is called in each round, except for the first one.
		/// </summary>
		/// <param name="gameRound">All players' moves in the previous round.</param>
		/// <returns>The move to play.</returns>
		string MakeMove(GameRound gameRound);
	}
}
